// Users Data Collection
{
	"_id": "user001",
	"firstName": "Arman",
	"lastName": "Villegas",
	"email" : "vilarman@mail.com",
	"password": "villArman",
	"isAdmin": true,
	"mobileNumber" : "09452411552",
	"dateTimeRegistered": "2022-06-10"

}

{
	"_id": "user002",
	"firstName": "Carlo",
	"lastName": "Perez",
	"email" : "carlop@mail.com",
	"password": "carl2536",
	"isAdmin": false,
	"mobileNumber" : "09854241111",
	"dateTimeRegistered": "2022-06-10"
	

}


// Orders Collection (Orders.json) 
{
	"_id" : "orders001",
	"userId": "user002",        
	"transactionDate":"2022-06-10",
	"Status": "Pending",
	"Total": 34300,
	"dateTimeRegistered": "2022-06-10"
}

// Products Collection (Products.json)

{
	"_id": "productsOrder001",
	"orderId": "orders001",
	"productId": "products001",
	"Quantity": 3,
	"price": 100,
	"subTotal": 300,
	"dateTimeCreated": "2022-06-10"
}

{
	"_id": "productsOrder002",
	"orderId": "orders002",
	"productId": "products002",
	"Quantity": 2,
	"price": 17000,
	"subTotal": 34000,
	"dateTimeCreated": "2022-06-10"
}



// Transaction Collection (Transaction.json)

{
	"_id": "products001",
	"productName": "Doreen",
	"description": "for baking purposes",
	"price": 100,
	"Stocks": 27,
	"isActive": true;
	"SKU":"Doreen-452"
	"dateTimeCreated": "2022-06-10"
}
{
	"_id": "products002",
	"productName": "Refrigerated",
	"description": "For Refrigerated Items",
	"price": 17000,
	"Stocks": 18,
	"isActive": true;
	"SKU":"ref-521"
	"dateTimeCreated": "2022-06-10"
}

// End



